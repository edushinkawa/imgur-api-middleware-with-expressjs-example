var express = require("express")
var request = require("request")
var app = express()

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  )
  next()
})

app.get(
  "/api/",
  (req, resource) => {
    const section = req.query.section ? req.query.section : "hot"
    const sort = req.query.sort ? req.query.sort : "viral"
    const window = req.query.window ? req.query.window : "day"
    const isViral = req.query.isViral ? req.query.isViral : true
    const options = {
      url: `https://api.imgur.com/3/gallery/${section}/${sort}/${window}?isViral=${isViral}`,
      method: "GET",
      headers: {
        "content-type": "application/json",
        Authorization: "Client-ID 82ed6aed9dd88b3"
      },
      json: true,
      timeout: 5000
    }
    request(options, (err, res, body) => {
      resource.json({
        author: {
          name: "Mobilabs",
          lastname: "Imgur Test"
        },
        items: body.data
      })
    })
  },
  (req, res) => {
    res.send("Imgur service error")
  }
)

app.listen(3009, () => {
  console.log("Mobilabs test app listening on port 3009!")
})
